package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	public int menorValor(IntegersBag bag) {
		int menor = Integer.MAX_VALUE;
		int value;
		if(bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()) {
				value = iter.next();
				if( menor > value){
					menor = value;
				}
			}
		}
		return menor;
	}
	public int multiplicar(IntegersBag bag) {
		int multiplicacion = 1;
		if(bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()) {
				multiplicacion *= iter.next();
			}
		}
		return multiplicacion;
	}
	public int sumar(IntegersBag bag) {
		int suma = 0;
		if(bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()) {
				suma += iter.next();
				
			}
		}
		return suma;
	}
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
}
